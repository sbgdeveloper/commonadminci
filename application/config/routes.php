<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'Frontend';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['login'] = 'admin/login';
$route['logout'] = 'admin/logout';
$route['admin/dashboard'] = 'backend/dashboard';

/*User */
$route['admin/user/list']='UserController';
$route['admin/user/add']='UserController/add';
$route['admin/user/edit/(:any)']='UserController/edit/$1';
$route['admin/user/enable/(:any)']='UserController/enable/$1';
$route['admin/user/disable/(:any)']='UserController/disable/$1';
$route['admin/user/delete/(:any)']='UserController/delete/$1';
$route['admin/change-password']='UserController/changePassword';

/*Pages */
$route['admin/pages/list']='PagesController';
$route['admin/pages/add']='PagesController/add';
$route['admin/pages/edit/(:any)']='PagesController/edit/$1';
$route['admin/pages/enable/(:any)']='PagesController/enable/$1';
$route['admin/pages/disable/(:any)']='PagesController/disable/$1';
$route['admin/pages/delete/(:any)']='PagesController/delete/$1';

/*Settings */
$route['admin/settings/list']='SettingsController';
$route['admin/settings/add']='SettingsController/add';
$route['admin/settings/edit/(:any)']='SettingsController/edit/$1';
$route['admin/settings/enable/(:any)']='SettingsController/enable/$1';
$route['admin/settings/disable/(:any)']='SettingsController/disable/$1';
$route['admin/settings/delete/(:any)']='SettingsController/delete/$1';


/*Blogs*/
$route['admin/blog/list']='BlogController';
$route['admin/blog/add']='BlogController/add';
$route['admin/blog/edit/(:any)']='BlogController/edit/$1';
$route['admin/blog/enable/(:any)']='BlogController/enable/$1';
$route['admin/blog/disable/(:any)']='BlogController/disable/$1';
$route['admin/blog/delete/(:any)']='BlogController/delete/$1';

/*Products*/
$route['admin/product/list']='ProductController';
$route['admin/product/add']='ProductController/add';
$route['admin/product/edit/(:any)']='ProductController/edit/$1';
$route['admin/product/enable/(:any)']='ProductController/enable/$1';
$route['admin/product/disable/(:any)']='ProductController/disable/$1';
$route['admin/product/delete/(:any)']='ProductController/delete/$1';

/*Client*/
$route['admin/client/list']='ClientController';
$route['admin/client/add']='ClientController/add';
$route['admin/client/edit/(:any)']='ClientController/edit/$1';
$route['admin/client/enable/(:any)']='ClientController/enable/$1';
$route['admin/client/disable/(:any)']='ClientController/disable/$1';
$route['admin/client/delete/(:any)']='ClientController/delete/$1';

/*Category*/
$route['admin/category/list']='CategoryController';
$route['admin/category/add']='CategoryController/add';
$route['admin/category/edit/(:any)']='CategoryController/edit/$1';
$route['admin/category/enable/(:any)']='CategoryController/enable/$1';
$route['admin/category/disable/(:any)']='CategoryController/disable/$1';
$route['admin/category/delete/(:any)']='CategoryController/delete/$1';

/*Sub Category*/
$route['admin/subcategory/list']='SubcategoryController';
$route['admin/subcategory/add']='SubcategoryController/add';
$route['admin/subcategory/edit/(:any)']='SubcategoryController/edit/$1';
$route['admin/subcategory/enable/(:any)']='SubcategoryController/enable/$1';
$route['admin/subcategory/disable/(:any)']='SubcategoryController/disable/$1';
$route['admin/subcategory/delete/(:any)']='SubcategoryController/delete/$1';