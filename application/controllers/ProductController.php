<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProductController extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('ProductModel');
		$this->load->library('upload');
	}

	public function index()
	{
		if ($this->session->userdata('logged_in')) {
			$data['Records'] = $this->ProductModel->getAll('products'); 
			$data['pagetitle'] = 'Product List';
			$this->load->view('product/list', $data);
		}else{
			redirect(base_url().'login');
		}
	}
	
	public function add()
	{
		if ($this->session->userdata('logged_in')) {
			if($this->input->post()){
				$config['upload_path'] = FCPATH .'/uploads/product/';
				$config['allowed_types'] = 'jpeg|jpg|png|gif';
				$config['max_size'] = 4096;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if ($this->upload->do_upload('image'))
				{
					$picture = $this->upload->data()['file_name'];
				}
				else
				{
					$picture = NULL;
				}

				$data = array(
					'name' =>$this->input->post('name') , 
					'image' =>$picture , 
					'description' =>$this->input->post('description') , 
					'seo_title' =>$this->input->post('seo_title') , 
					'seo_description' =>$this->input->post('seo_description') , 
					'seo_keywords' =>$this->input->post('seo_keywords') , 
					'cannonical_link' =>$this->input->post('cannonical_link') , 
					'slug' =>$this->input->post('slug') , 
					'status' =>$this->input->post('status') , 
					'created_at'=>date("Y-m-d H:i:s"),
					'created_by' => $this->session->userdata('username'), 

				);
				if ($this->ProductModel->add($data, 'products'))
				{
					$this->session->set_flashdata('msg', 'Product Added Successfully');
				}
				else
				{
					$this->session->set_flashdata('msg', 'Error Adding Product');
				}

				redirect(base_url().'admin/product/add');

			}else{
				$data['pagetitle'] = 'Add Product';
				$this->load->view('product/add', $data);			
			}
		}else{
			redirect(base_url().'login');
		}
	}


	public function edit($id)
	{
		if ($this->session->userdata('logged_in')) {
			if($this->input->post()){
				$config['upload_path'] = FCPATH .'/uploads/product/';
				$config['allowed_types'] = 'jpeg|jpg|png|gif';
				$config['max_size'] = 4096;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if ($this->upload->do_upload('image'))
				{
					$picture = $this->upload->data()['file_name'];
					$data = array(
						'name' =>$this->input->post('name') , 
						'image' =>$picture , 
						'description' =>$this->input->post('description') , 
						'seo_title' =>$this->input->post('seo_title') , 
						'seo_description' =>$this->input->post('seo_description') , 
						'seo_keywords' =>$this->input->post('seo_keywords') , 
						'cannonical_link' =>$this->input->post('cannonical_link') , 
						'slug' =>$this->input->post('slug') , 
						'updated_at'=>date("Y-m-d H:i:s"),
						'updated_by' => $this->session->userdata('username'), 

					);
				}
				else
				{
					$data = array(
						'name' =>$this->input->post('name') , 
						'description' =>$this->input->post('description') , 
						'seo_title' =>$this->input->post('seo_title') , 
						'seo_description' =>$this->input->post('seo_description') , 
						'seo_keywords' =>$this->input->post('seo_keywords') , 
						'cannonical_link' =>$this->input->post('cannonical_link') , 
						'slug' =>$this->input->post('slug') , 
						'updated_at'=>date("Y-m-d H:i:s"),
						'updated_by' => $this->session->userdata('username'), 

					);
				}


				
				if ($this->ProductModel->edit($data, 'products', $id))
				{
					$this->session->set_flashdata('msg', 'Product Edited Successfully');
				}
				else
				{
					$this->session->set_flashdata('msg', 'Error Editing Product');
				}
				redirect(base_url().'admin/product/list');

			}else{
				$data['pagetitle'] = 'Edit Product';
				$data['Record'] = $this->ProductModel->getById('products', $id);
				$this->load->view('product/edit', $data);			
			}
		}else{
			redirect(base_url().'login');
		}
	}

	public function delete($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->ProductModel->delete('products', $id))
			{
				$this->session->set_flashdata('msg', 'Product Deleted Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Deleting Product');
			}
			redirect(base_url().'admin/product/list');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function enable($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->ProductModel->enable('products', $id))
			{
				$this->session->set_flashdata('msg', 'Product Enabled Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Enabling Product');
			}
			redirect(base_url().'admin/product/list');			
		}
		else
		{
			redirect(base_url());
		}
	}

	public function disable($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->ProductModel->disable('products', $id))
			{
				$this->session->set_flashdata('msg', 'Product Disabled Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Disabling Product');
			}
			redirect(base_url().'admin/product/list');			
		}
		else
		{
			redirect(base_url());
		}
	}

}
