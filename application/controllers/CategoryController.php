<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CategoryController extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('CategoryModel');
		$this->load->library('upload');
	}

	public function index()
	{
		if ($this->session->userdata('logged_in')) {
			$data['Records'] = $this->CategoryModel->getAll('category'); 
			$data['pagetitle'] = 'Category List';
			$this->load->view('category/list', $data);
		}else{
			redirect(base_url().'login');
		}
	}
	
	public function add()
	{
		if ($this->session->userdata('logged_in')) {
			if($this->input->post()){
				$config['upload_path'] = FCPATH .'/uploads/category/';
				$config['allowed_types'] = 'jpeg|jpg|png|gif';
				$config['max_size'] = 4096;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if ($this->upload->do_upload('image'))
				{
					$picture = $this->upload->data()['file_name'];
				}
				else
				{
					$picture = NULL;
				}

				$data = array(
					'name' =>$this->input->post('name') , 
					'image' =>$picture , 
					'description' =>$this->input->post('description') , 
					'seo_title' =>$this->input->post('seo_title') , 
					'seo_description' =>$this->input->post('seo_description') , 
					'seo_keywords' =>$this->input->post('seo_keywords') , 
					'cannonical_link' =>$this->input->post('cannonical_link') , 
					'slug' =>$this->input->post('slug') , 
					'status' =>$this->input->post('status') , 
					'created_at'=>date("Y-m-d H:i:s"),
					'created_by' => $this->session->userdata('username'), 

				);
				if ($this->CategoryModel->add($data, 'category'))
				{
					$this->session->set_flashdata('msg', 'Category Added Successfully');
				}
				else
				{
					$this->session->set_flashdata('msg', 'Error Adding Category');
				}

				redirect(base_url().'admin/category/add');

			}else{
				$data['pagetitle'] = 'Add Category';
				$this->load->view('category/add', $data);			
			}
		}else{
			redirect(base_url().'login');
		}
	}


	public function edit($id)
	{
		if ($this->session->userdata('logged_in')) {
			if($this->input->post()){
				$config['upload_path'] = FCPATH .'/uploads/category/';
				$config['allowed_types'] = 'jpeg|jpg|png|gif';
				$config['max_size'] = 4096;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if ($this->upload->do_upload('image'))
				{
					$picture = $this->upload->data()['file_name'];
					$data = array(
						'name' =>$this->input->post('name') ,
						'image' =>$picture , 						
						'description' =>$this->input->post('description') , 
						'seo_title' =>$this->input->post('seo_title') , 
						'seo_description' =>$this->input->post('seo_description') , 
						'seo_keywords' =>$this->input->post('seo_keywords') , 
						'cannonical_link' =>$this->input->post('cannonical_link') , 
						'slug' =>$this->input->post('slug') , 
						'updated_at'=>date("Y-m-d H:i:s"),
						'updated_by' => $this->session->userdata('username'), 

					);
				}
				else
				{
					$data = array(
						'name' =>$this->input->post('name') ,
						'description' =>$this->input->post('description') , 
						'seo_title' =>$this->input->post('seo_title') , 
						'seo_description' =>$this->input->post('seo_description') , 
						'seo_keywords' =>$this->input->post('seo_keywords') , 
						'cannonical_link' =>$this->input->post('cannonical_link') , 
						'slug' =>$this->input->post('slug') , 
						'updated_at'=>date("Y-m-d H:i:s"),
						'updated_by' => $this->session->userdata('username'), 

					);
				}


				
				if ($this->CategoryModel->edit($data, 'category', $id))
				{
					$this->session->set_flashdata('msg', 'Record Edited Successfully');
				}
				else
				{
					$this->session->set_flashdata('msg', 'Error Editing Record');
				}
				redirect(base_url().'admin/category/list');

			}else{
				$data['pagetitle'] = 'Edit Category';
				$data['Record'] = $this->CategoryModel->getById('category', $id);
				$this->load->view('category/edit', $data);			
			}
		}else{
			redirect(base_url().'login');
		}
	}

	public function delete($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->CategoryModel->delete('category', $id))
			{
				$this->session->set_flashdata('msg', 'Record Deleted Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Deleting Record');
			}
			redirect(base_url().'admin/category/list');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function enable($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->CategoryModel->enable('category', $id))
			{
				$this->session->set_flashdata('msg', 'Record Enabled Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Enabling Record');
			}
			redirect(base_url().'admin/category/list');			
		}
		else
		{
			redirect(base_url());
		}
	}

	public function disable($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->CategoryModel->disable('category', $id))
			{
				$this->session->set_flashdata('msg', 'Record Disabled Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Disabling Record');
			}
			redirect(base_url().'admin/category/list');			
		}
		else
		{
			redirect(base_url());
		}
	}

}
