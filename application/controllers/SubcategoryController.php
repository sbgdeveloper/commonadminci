<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SubcategoryController extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('SubcategoryModel');
		$this->load->library('upload');
	}

	public function index()
	{
		if ($this->session->userdata('logged_in')) {
			$data['Records'] = $this->SubcategoryModel->getAll('subcategory'); 
			$data['pagetitle'] = 'Sub Category List';
			$this->load->view('subcategory/list', $data);
		}else{
			redirect(base_url().'login');
		}
	}
	
	public function add()
	{
		if ($this->session->userdata('logged_in')) {
			if($this->input->post()){
				$config['upload_path'] = FCPATH .'/uploads/subcategory/';
				$config['allowed_types'] = 'jpeg|jpg|png|gif';
				$config['max_size'] = 4096;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if ($this->upload->do_upload('image'))
				{
					$picture = $this->upload->data()['file_name'];
				}
				else
				{
					$picture = NULL;
				}

				$data = array(
					'name' =>$this->input->post('name') , 
					'category_id' =>$this->input->post('category_id') , 
					'image' =>$picture , 
					'description' =>$this->input->post('description') , 
					'seo_title' =>$this->input->post('seo_title') , 
					'seo_description' =>$this->input->post('seo_description') , 
					'seo_keywords' =>$this->input->post('seo_keywords') , 
					'cannonical_link' =>$this->input->post('cannonical_link') , 
					'slug' =>$this->input->post('slug') , 
					'status' =>$this->input->post('status') , 
					'created_at'=>date("Y-m-d H:i:s"),
					'created_by' => $this->session->userdata('username'), 

				);
				if ($this->SubcategoryModel->add($data, 'subcategory'))
				{
					$this->session->set_flashdata('msg', 'Record Added Successfully');
				}
				else
				{
					$this->session->set_flashdata('msg', 'Error Adding Record');
				}

				redirect(base_url().'admin/subcategory/add');

			}else{
				$data['pagetitle'] = 'Add Sub Category';
				$categories= $this->SubcategoryModel->getAllActive('category');
				if (empty($categories)) {
					$this->session->set_flashdata('err', 'Please add Category First');
					redirect(base_url().'admin/category/add');

				}
				$data['categories']=$categories;
				$this->load->view('subcategory/add', $data);			
			}
		}else{
			redirect(base_url().'login');
		}
	}


	public function edit($id)
	{
		if ($this->session->userdata('logged_in')) {
			if($this->input->post()){
				$config['upload_path'] = FCPATH .'/uploads/subcategory/';
				$config['allowed_types'] = 'jpeg|jpg|png|gif';
				$config['max_size'] = 4096;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if ($this->upload->do_upload('image'))
				{
					$picture = $this->upload->data()['file_name'];
					$data = array(
						'name' =>$this->input->post('name') , 
						'image' =>$picture , 
						'category_id' =>$this->input->post('category_id') , 
						'description' =>$this->input->post('description') , 
						'seo_title' =>$this->input->post('seo_title') , 
						'seo_description' =>$this->input->post('seo_description') , 
						'seo_keywords' =>$this->input->post('seo_keywords') , 
						'cannonical_link' =>$this->input->post('cannonical_link') , 
						'slug' =>$this->input->post('slug') , 
						'updated_at'=>date("Y-m-d H:i:s"),
						'updated_by' => $this->session->userdata('username'), 
					);
				}
				else
				{
					$data = array(
						'name' =>$this->input->post('name') , 
						'category_id' =>$this->input->post('category_id') , 
						'description' =>$this->input->post('description') , 
						'seo_title' =>$this->input->post('seo_title') , 
						'seo_description' =>$this->input->post('seo_description') , 
						'seo_keywords' =>$this->input->post('seo_keywords') , 
						'cannonical_link' =>$this->input->post('cannonical_link') , 
						'slug' =>$this->input->post('slug') , 
						'updated_at'=>date("Y-m-d H:i:s"),
						'updated_by' => $this->session->userdata('username'), 
					);
				}

				if ($this->SubcategoryModel->edit($data, 'subcategory', $id))
				{
					$this->session->set_flashdata('msg', 'Record Edited Successfully');
				}
				else
				{
					$this->session->set_flashdata('msg', 'Error Editing Record');
				}
				redirect(base_url().'admin/subcategory/list');

			}else{
				$data['pagetitle'] = 'Edit Sub Category';
				$data['categories']= $this->SubcategoryModel->getAllActive('category');
				$data['Record'] = $this->SubcategoryModel->getById('subcategory', $id);
				$this->load->view('subcategory/edit', $data);			
			}
		}else{
			redirect(base_url().'login');
		}
	}

	public function delete($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->SubcategoryModel->delete('subcategory', $id))
			{
				$this->session->set_flashdata('msg', 'Sub Category Deleted Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Deleting Sub Category');
			}
			redirect(base_url().'admin/subcategory/list');
		}
		else
		{
			redirect(base_url());
		}
	}

	public function enable($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->SubcategoryModel->enable('subcategory', $id))
			{
				$this->session->set_flashdata('msg', 'Sub Category Enabled Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Enabling Sub Category');
			}
			redirect(base_url().'admin/subcategory/list');			
		}
		else
		{
			redirect(base_url());
		}
	}

	public function disable($id)
	{
		if ($this->session->userdata('logged_in'))
		{
			if ($this->SubcategoryModel->disable('subcategory', $id))
			{
				$this->session->set_flashdata('msg', 'Sub Category Disabled Successfully');
			}
			else
			{
				$this->session->set_flashdata('msg', 'Error Disabling Sub Category');
			}
			redirect(base_url().'admin/subcategory/list');			
		}
		else
		{
			redirect(base_url());
		}
	}

}
