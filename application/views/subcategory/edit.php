<?php $this->load->view('layouts/header');?>
<?php $this->load->view('layouts/sidebar');?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Sub Category Management
			<small>Edit Sub Category</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?=base_url()?>admin/subcategory/list">Sub Category</a></li>
			<li class="active">Edit Sub Category</li>
		</ol>
	</section>
	<section class="content">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title"><?=$pagetitle;?></h3>
			</div>
			<div class="box-body">
				<div class="row">
					<form method="post" enctype="multipart/form-data">
						<div class="col-md-12">
							<div class="form-group">
								<label>Category</label>
								<select name="category_id" class="form-control select2" data-placeholder="" style="width: 100%;">
									<?php if ($categories): ?>
										<?php foreach ($categories as $category): ?>
											<?php if ($category['id']==$Record['category_id']): ?>
												<option value="<?= $category['id'] ?>" selected><?= $category['name'] ?></option>
												<?php else: ?>

													<option value="<?= $category['id'] ?>"><?= $category['name'] ?></option>
												<?php endif ?>

											<?php endforeach ?>
											<?php else: ?>
												<option value="1">No Category Added</option>
											<?php endif ?>
										</select>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label>Sub Category Name *</label>
										<input type="text" name="name" class="form-control" placeholder="Enter Name" value="<?=$Record['name']?>" required>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label>Image</label>
										<input type='file' class="form-control" name="image" onchange="readURL(this);" value="<?=$Record['image']?>" >
										<?php if (isset($Record['image'])): ?>
											<img id="blah" src="<?= base_url('uploads/subcategory')?>/<?=$Record['image']?>" alt="your image" class="pre-img" />
											<?php else: ?>
												<img id="blah" src="http://placehold.it/180" alt="your image" class="pre-img" />
											<?php endif ?>

										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label>Description *</label>
											<textarea id="editor1" name='description' placeholder="Enter Description">
												<?=$Record['description']?></textarea>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<label>Meta Title *</label>
												<input type="text" name="seo_title" class="form-control" placeholder="Enter  Meta Title" value="<?=$Record['seo_title']?>" required>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<label> Meta Description *</label>
												<input type="text" name="seo_description" class="form-control" placeholder="Enter  Meta Description" value="<?=$Record['seo_description']?>" required>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<label> Meta Keyword *</label>
												<input type="text" name="seo_keywords" class="form-control" placeholder="Enter  Meta Keywords" value="<?=$Record['seo_keywords']?>" required>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<label> URL *</label>
												<input type="text" name="slug" class="form-control" placeholder="Enter  URL" value="<?=$Record['slug']?>" required>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<label> Canonical/URL *</label>
												<input type="text" name="cannonical_link" class="form-control" placeholder="Enter Canonical URL" value="<?=$Record['cannonical_link']?>" required>
											</div>
										</div>
										<div class="col-md-12">
											<button type="submit" class="btn btn-primary">Submit</button>
										</div>
									</form>

								</div>
							</div>
							<!-- /.row -->
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</section>
				<!-- /.content -->
			</div>
			<?php $this->load->view('layouts/footer');?>