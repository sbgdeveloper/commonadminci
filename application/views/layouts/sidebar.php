<aside class="main-sidebar">
	<section class="sidebar">
		<ul class="sidebar-menu" data-widget="tree">
			<li class="header">MAIN NAVIGATION</li>
			<li><a href="<?=base_url()?>admin/dashboard"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
			<li><a href="<?=base_url()?>admin/pages/list"><i class="fa fa-file"></i> <span>Pages</span></a></li>
			<li><a href="<?=base_url()?>admin/blog/list"><i class="fa fa-newspaper-o"></i> <span>Blogs</span></a></li>
			<li><a href="<?=base_url()?>admin/category/list"><i class="fa fa-tag"></i> <span>Category</span></a></li>
			<li><a href="<?=base_url()?>admin/subcategory/list"><i class="fa fa-list-alt"></i> <span>Sub Category</span></a></li>
			<li><a href="<?=base_url()?>admin/product/list"><i class="fa fa-product-hunt"></i> <span>Product</span></a></li>
			<li><a href="<?=base_url()?>admin/client/list"><i class="fa fa-user-circle-o"></i> <span>Clients</span></a></li>
			<li><a href="<?=base_url()?>admin/user/list"><i class="fa fa-user"></i> <span>User Managements</span></a></li>
			<li><a href="<?=base_url()?>admin/settings/list"><i class="fa fa-gear"></i> <span>Settings</span></a></li>
			
		</ul>
	</section>
</aside>