<?php $this->load->view('layouts/header');?>
<?php $this->load->view('layouts/sidebar');?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Blogs Management
			<small>Add Blog</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?=base_url()?>admin/blog/list">Blogs</a></li>
			<li class="active">Add Blog</li>
		</ol>
	</section>
	<section class="content">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title"><?=$pagetitle;?></h3>
			</div>
			<div class="box-body">
				<div class="row">
					<form method="post" enctype="multipart/form-data">
						<div class="col-md-12">
							<div class="form-group">
								<label>Blog Image</label>
								<input type='file'  name="blog_image" onchange="readURL(this);" required>	
								<img id="blah" src="http://placehold.it/180" alt="your image" class="pre-img" />
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Blog Title *</label>
								<input type="text" name="blog_title" class="form-control" placeholder="Enter Blog Title...." required>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Category</label>
								<select name="category" class="form-control select2" data-placeholder="Select a Category" style="width: 100%;">
									<option>News</option>
									<option>Science</option>
									<option>Healthcare</option>
									<option>Technology</option>
								</select>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Description *</label>
								<textarea id="editor1" name='description'>
								</textarea>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Blog Meta Title *</label>
								<input type="text" name="blog_metatitle" class="form-control" placeholder="Enter Blog Meta Title...." required>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Blog Meta Description *</label>
								<input type="text" name="blog_metadesc" class="form-control" placeholder="Enter Blog Meta Description...." required>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Blog Meta Keyword *</label>
								<input type="text" name="blog_metakeyword" class="form-control" placeholder="Enter Blog Meta Keywords...." required>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Blog Canonical/URL *</label>
								<input type="text" name="slug" class="form-control" placeholder="Enter Blog URL...." required>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Blog Schema</label>
								<input type="text" name="blog_schema" class="form-control" placeholder="Enter Blog Schema...." required>
							</div>
						</div>
						<div class="col-md-12">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
</div>
<?php $this->load->view('layouts/footer');?>