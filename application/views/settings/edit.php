<?php $this->load->view('layouts/header');?>
<?php $this->load->view('layouts/sidebar');?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Setting Management
			<small>Edit Setting</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?=base_url()?>admin/setting/list">Setting</a></li>
			<li class="active">Edit Setting</li>
		</ol>
	</section>
	<section class="content">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title"><?=$pagetitle;?></h3>
			</div>
			<div class="box-body">
				<div class="row">
					<form method="post" enctype="multipart/form-data">
						<div class="col-md-12">
							<div class="form-group">
								<label>App Name *</label>
								<input type="text" name="app_name" class="form-control" placeholder="Enter  Name" value="<?=$Record['app_name']?>">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Email *</label>
								<input type="email" name="email" class="form-control" placeholder="Enter  Email" value="<?=$Record['email']?>">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Phone Number *</label>
								<input type="text" name="phone" class="form-control" placeholder="Enter  Phone Number" value="<?=$Record['phone']?>">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Address *</label>
								<textarea name="address" id="editor1" class="form-control" rows="3" placeholder="Enter Address"><?=$Record['address']?></textarea>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Header Files *</label>
								<textarea name="header_files" id="editor2" class="form-control" rows="3" placeholder="Enter Code For <head> </head>"><?=$Record['header_files']?></textarea>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Facebook Link*</label>
								<input type="text" name="facebook" class="form-control" placeholder="Enter  Facebook Link" value="<?=$Record['facebook']?>">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Linkedin Link *</label>
								<input type="text" name="linkdin" class="form-control" placeholder="Enter  Linkedin Link" value="<?=$Record['linkdin']?>">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Twitter Link *</label>
								<input type="text" name="twitter" class="form-control" placeholder="Enter  Twitter Link" value="<?=$Record['twitter']?>">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Google Plus Link *</label>
								<input type="text" name="google_plus" class="form-control" placeholder="Google Plus Link" value="<?=$Record['google_plus']?>">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Instagram Link *</label>
								<input type="text" name="instagram" class="form-control" placeholder="Enter  Instagram Link " value="<?=$Record['instagram']?>">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>App URL *</label>
								<input type="text" name="url" class="form-control" placeholder="Enter Blog URL" value="<?=$Record['url']?>">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label>Status</label>
								<select name="status" class="form-control select2" data-placeholder="" style="width: 100%;">
									<option value="1">Active</option>
									<option value="0">Inactive</option>
								</select>
							</div>
						</div>
						<div class="col-md-12">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</div>
					<!-- /.col -->
				</form>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
	</div>
	<!-- /.box -->
</section>
<!-- /.content -->
</div>
<!-- <script>tinymce.init({selector:'textarea',inline_styles : false});</script> -->
<?php $this->load->view('layouts/footer');?>